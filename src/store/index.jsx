import { createStore, combineReducers,  applyMiddleware } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import librosReducer from './modules/libros';
import rootReducer from './modules/auth';

const reducers =combineReducers({
    libro: librosReducer,
    auth: rootReducer,
});

const store = createStore(    
    reducers,
    applyMiddleware(thunk,logger)
);

export default store;
