import { START, SUCCESS, ERROR } from './libros.const';
import { librosGetAll, librosCreate, librosUpdate, librosDelete } from '../../../services/libros.service';

const librosStartAction = () => ({ type: START, payload: null });
const librosSuccessAction = (data) => ({ type: SUCCESS, payload: data });
const librosErrorAction = (message) => ({ type: ERROR, payload: message });

const libroActionCreateStart = () => ({ type: START, payload: null });
// const libroActionCreateSucces = (data) => ({ type: SUCCESS, payload: data });
const libroActionCreateError = (data) => ({ type: ERROR, payload: data });

const libroActionDeleteStart = () => ({ type: START, payload: null });
const libroActionDeleteSucces = (data) => ({ type: SUCCESS, payload: null });
const libroActionDeleteError = (data) => ({ type: ERROR, payload: data });

const libroActionUpdateStart = () => ({ type: START, payload: null });
const libroActionUpdateSucces = (data) => ({ type: SUCCESS, payload: null });
const libroActionUpdateError = (data) => ({ type: ERROR, payload: data });

export const librosAsyncActionGetAll = () => {
    return (dispatch) => {
        dispatch(librosStartAction());
        const token=localStorage.getItem('toke');
        librosGetAll(token).then(res => {
            dispatch(librosSuccessAction(res.data.data));
        }).catch((err) => {
            dispatch(librosErrorAction(err.data))
        })
    }
}

export const librosAsyncAtionCreate = (data) => {
    return (dispatch) => {
        dispatch(libroActionCreateStart());
        const token = localStorage.getItem('toke');
        librosCreate(data, token).then(res => {
            dispatch(librosAsyncActionGetAll());
        }).catch(error => {
            dispatch(libroActionCreateError(error.data));
        });
    }
}

export const librosAsyncAtionEdit = (data) => {
    return (dispatch) => {
        dispatch(libroActionUpdateStart());
        const token = localStorage.getItem('toke');
        librosUpdate(data, token).then(res => {
            dispatch(librosAsyncActionGetAll(res.data.data));
            dispatch(libroActionUpdateSucces(res.data.data));
        }).catch(error => {
            dispatch(libroActionUpdateError(error));
        });
    }
}

export const libroAsyncAtionDelete = (data) => {
    return (dispatch) => {
        dispatch(libroActionDeleteStart());
        const token = localStorage.getItem('toke');
        librosDelete(data, token).then(res => {
            dispatch(librosAsyncActionGetAll());
            dispatch(libroActionDeleteSucces(res.data.data));
        }).catch(error => {
            dispatch(libroActionDeleteError(error));
        });
    }
}