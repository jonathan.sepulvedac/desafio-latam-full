import { combineReducers } from 'redux';

import libroReducer from './libros.reducer';

const librosReducer = combineReducers({
    librosGetAll: libroReducer,
});

export default librosReducer;