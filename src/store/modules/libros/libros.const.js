const MODULE = 'libros';
const ACTION = 'libros';

export const START = `${MODULE}/${ACTION}/start`;
export const SUCCESS = `${MODULE}/${ACTION}/success`;
export const ERROR = `${MODULE}/${ACTION}/error`;
