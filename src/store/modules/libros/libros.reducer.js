import { START, SUCCESS, ERROR } from './libros.const';

const initialState = {
    loading: false,
    data: null,
    success: null,
    error: null,
    errorMessage: ''
};

const libroReducer = (prevState = initialState, action) => {
    console.log(action.type);
    switch (action.type) {
        case START:
            return {
                ...prevState,
                loading: true,
            }
        case SUCCESS:
            return {
                ...prevState,
                error: false,
                loading: false,
                success: true,
                data: action.payload,
            }
        case ERROR:
            return {
                ...prevState,
                error: true,
                loading: false,
                success: false,
                errorMessage: action.payload,
            }
        default:
            return prevState;
    }
};

export default libroReducer;