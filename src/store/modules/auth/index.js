import { combineReducers } from 'redux';

import loginReducer from './login.reducer';


const rootReducer = combineReducers({
    auth:loginReducer,
});

export default rootReducer;