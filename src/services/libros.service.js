import axios from 'axios';
import { API_HOST } from './config';

class ApiError extends Error { }

export const librosGetAll = async (token) => {
    try {
        return await axios.get(`${API_HOST}/api/libros`, {
            headers: {
                authorization: `bearer ${token}`,
            },
        });
    } catch (error) {
        const status = error.response.status;
        if (status === 404) throw new ApiError('404');
        throw new ApiError(error.message);
    }
};

export const librosCreate = async (data,token) => {
    try {
        return await axios.post(`${API_HOST}/api/libros/`, data,{
            headers: {
                authorization: `bearer ${token}`,
                data: data,
            },
        });
    } catch (error) {
        const status = error.response.status;
        if (status === 404) throw new ApiError('404');
        throw new ApiError(error.message);
    }
};

export const librosUpdate = async (data,token) => {
    try {
        return await axios.put(`${API_HOST}/api/libros/${data.id}`,data,{
            headers: {
                authorization: `bearer ${token}`,
                data: data,
            },
        });
    } catch (error) {
        const status = error.response;
        if (status === 404) throw new ApiError('404');
        throw new ApiError(error.message);
    }
};

export const librosDelete = async (data,token) => {
    try {
        return await axios.delete(`${API_HOST}/api/libros/${data.id}`,{
            headers: {
                authorization: `bearer ${token}`
            },
        });
    } catch (error) {
        const status = error.response;
        if (status === 404) throw new ApiError('404');
        throw new ApiError(error.message);
    }
};

