import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Container, Table, Card, CardHeader, CardBody, Button, Col } from 'reactstrap';
import { Link } from "react-router-dom";
import { FaPlusCircle, FaPencilAlt, FaTrashAlt } from 'react-icons/fa';
import { librosAsyncActionGetAll, libroAsyncAtionDelete } from '../../store/modules/libros/libros.action';
import Loader from 'react-loader-spinner';


const Home = (props) => {
    const dispatch = useDispatch();

    const handleDelete = (e) => {
        return () => {
            dispatch(libroAsyncAtionDelete(e));
            props.history.push('/dashboard/libro');
        }
    }

    useEffect(() => {
        dispatch(librosAsyncActionGetAll());
    }, [dispatch]);

    const data = useSelector(datos => datos.libro.librosGetAll.data);

    return (
        <Container>
            <Col className="mt-5">
                <Card>
                    <CardHeader className="text-right">
                        <Link to="/dashboard/createlibro"> <Button style={{ color: 'white' }} color="warning"> Agregar Producto <FaPlusCircle /> </Button> </Link>
                    </CardHeader>
                    <CardBody className="align-self-center">

                        {data ? <Table responsive>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>Tipo</th>
                                    <th>Precio</th>
                                    <th />
                                    <th />
                                </tr>
                            </thead>
                            <tbody>
                                {data.map((e) => (
                                    <tr key={e.id}>
                                        <td>{e.id}</td>
                                        <td>{e.nombre}</td>
                                        <td>{e.tipo}</td>
                                        <td>{e.precio}</td>
                                        <td><Link to={{
                                            pathname: '/dashboard/updatelibro', state: {
                                                data: e
                                            }
                                        }}>
                                            <Button color="primary">Modificar <FaPencilAlt /></Button>
                                        </Link>
                                        </td>
                                        <td><Button color='danger' onClick={handleDelete(e)}>Eliminar <FaTrashAlt /></Button></td>
                                    </tr>


                                ))}
                            </tbody>



                        </Table>:
                        <Loader
                            className="mt-5 mb-5"                            
                            type="CradleLoader"
                            color="#00BFFF"
                            height={200}
                            width={200}
                        //timeout={2000} //3 secs
                        />
                        }

                    </CardBody>
                </Card>
            </Col>
        </Container>
    )
};

export default Home;