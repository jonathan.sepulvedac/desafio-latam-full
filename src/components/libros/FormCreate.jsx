import React from 'react';
import { useDispatch } from 'react-redux';
import { Container, Col, Form, FormGroup, Input, Button, Label, Card, CardHeader, CardBody, CardFooter } from 'reactstrap';
import useGenericInput from './../../hooks/useGenericInput';
import { librosAsyncAtionCreate } from '../../store/modules/libros/libros.action';




const FormCreate = (props) => {
    const dispatch = useDispatch();

    const nombre = useGenericInput('', 'text');
    const tipo = useGenericInput('', 'text');
    const precio = useGenericInput('', 'text')
    const buttonIsDisabled = () => nombre.value === '' || tipo.value === '' || precio.value === '';

    const handlerSubmit = (e) => {
        e.preventDefault();
        let array = {
            "nombre": nombre.value,
            "tipo": tipo.value,
            "precio": precio.value
        }
        dispatch(librosAsyncAtionCreate(array));
        props.history.push('/dashboard/libro');
    }

    return (
        <Container >
            <Col sm={8} md={7} lg={6} className="mx-auto mt-5">  
                <Card sm={6}>
                    <CardHeader>Ingreso de Libro</CardHeader>
                    <Form onSubmit={handlerSubmit}>
                        <CardBody>
                            <FormGroup row>
                                <Label sm={4}>Nombre</Label>
                                <Col sm={8}>
                                    <Input {...nombre} />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={4}>Tipo</Label>
                                <Col sm={8}>
                                    <Input sm={8} {...tipo} />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={4}>Precio</Label>
                                <Col sm={8}>
                                    <Input sm={8} {...precio} />
                                </Col>
                            </FormGroup>
                        </CardBody>
                        <CardFooter className="text-center"><Button color='primary' type='submit' disabled={buttonIsDisabled()}>Crear</Button></CardFooter>
                    </Form>
                </Card>
            </Col>
        </Container>

    )
}

export default FormCreate;