import React from 'react';
import { useDispatch } from 'react-redux';
import { Container, Col, Form, FormGroup, Input, Button, Label, Card, CardHeader, CardBody, CardFooter } from 'reactstrap';
import useGenericInput from '../../hooks/useGenericInput';
import { librosAsyncAtionEdit } from '../../store/modules/libros/libros.action';

const FormEdit = (props) => {

    const valor = props.location.state;
    const id = valor.data.id;
    const nombre = useGenericInput(valor.data.nombre, 'text');
    const tipo = useGenericInput(valor.data.tipo, 'text');
    const precio = useGenericInput(valor.data.precio, 'text')
    const buttonIsDisabled = () => nombre.value === '' || tipo.value === '' || precio.value === '';
    const dispatch = useDispatch();

    const handlerSubmit = (e) => {
        e.preventDefault();
        let array = {
            "id": id,
            "nombre": nombre.value,
            "tipo": tipo.value,
            "precio": precio.value
        }
        dispatch(librosAsyncAtionEdit(array));
        props.history.push('/dashboard/libro');
    }

    return (
        <Container>
            <Col sm={8} md={7} lg={6} className="mx-auto mt-5">
                <Card>
                    <CardHeader>Editar Libro</CardHeader>
                    <Form onSubmit={handlerSubmit}>
                        <CardBody>
                            <FormGroup row>
                                <Label sm={4}>Nombre</Label>
                                <Col sm={8}>
                                    <Input {...nombre} />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={4}>Tipo</Label>
                                <Col sm={8}>
                                    <Input sm={8} {...tipo} />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={4}>Precio</Label>
                                <Col sm={8}>
                                    <Input sm={8} {...precio} />
                                </Col>
                            </FormGroup>
                        </CardBody>
                        <CardFooter className="text-center"><Button color='primary' type='submit' disabled={buttonIsDisabled()}>Guardar cambios</Button></CardFooter>
                    </Form>
                </Card>
            </Col>
        </Container>
    )
}

export default FormEdit;