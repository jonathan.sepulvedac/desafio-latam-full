import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { NavLink as RRNavLink } from 'react-router-dom';
import { logoutActionCreator } from './../../store/modules/auth/login.action';
// import { FaRegStar } from 'react-icons/fa';
import '.././../App.css';

import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap';

const Header = (props) => {
  const dispatch = useDispatch();
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => {
    setIsOpen(!isOpen);
  }

  const logout = () => {
    dispatch(logoutActionCreator());
    props.history.push('/login')
  }
  return (
    <div className="header-perso">
      <Navbar light expand="md">
        <NavbarBrand href="/dashboard/welcome" className="header-text">Desafio-React</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="ml-auto" navbar>
            <UncontrolledDropdown className="header-perso" nav inNavbar>
              <DropdownToggle nav caret className="header-text">
                Opciones
                </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem>
                  <NavItem>
                    <NavLink tag={RRNavLink} exact to="/dashboard/libro" activeClassName="active">Listado de Libros</NavLink>
                  </NavItem>
                </DropdownItem>
                <DropdownItem divider />
                <DropdownItem onClick={logout}>
                  Cerrar Sesión
                  </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </Nav>
        </Collapse>
      </Navbar>
    </div>
  );
}

export default Header;




