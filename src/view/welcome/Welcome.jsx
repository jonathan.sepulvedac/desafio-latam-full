import React from 'react';
import logo from '../../react.png';
const Welcome = (props) => {
    return (
        <div className="user-list-view welcome-perso text-center">
            <img src={logo} className="App-logo" alt="logo" />
            <div className="thumb">
                <a href="/" style={{ pointerEvents: "none" }}>
                    <span>Bienvenid@ </span>
                </a>
            </div>
        </div>
    );
};

export default Welcome;