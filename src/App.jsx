import React from 'react';
import store from './store';
import { Provider } from 'react-redux';
import Home from './components/libros/Home';
import Header from './components/header/Header';
import FormCreate from './components/libros/FormCreate';
import FormEdit from './components/libros/FormEdit';
import { BrowserRouter as Router, Route } from "react-router-dom";
import PrivateRoute from './components/private-route/PrivateRoute'
import './App.css';
import Login from './view/login/Login';
import Welcome from './view/welcome/Welcome';

function App() {

  return (
    <div className="App my-auto" height="100%">
      <Provider store={store}>
        <Router>
          <Route path="/" exact component={Login} />
          <PrivateRoute path="/dashboard" component={Header} />
          <PrivateRoute path="/dashboard/welcome" exact component={Welcome} />
          <Route path="/login" exact component={Login} />
          <PrivateRoute path="/dashboard/createlibro" exact component={FormCreate} />
          <PrivateRoute path="/dashboard/updatelibro" exact component={FormEdit} />
          <PrivateRoute path="/dashboard/libro" exact component={Home} />
        </Router>
      </Provider>

    </div>
  );
}

export default App;
