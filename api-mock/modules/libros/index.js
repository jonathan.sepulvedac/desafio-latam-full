var middleware = require("../../jwt/middleware");
const { encryptPassword } = require('../../crypto');

/**
 * 
 * @param {Express} app 
 * @param {Database} db 
 */
const librosModule = (app, db) => {

    app.get("/api/libros", (req, res, next) => {
        var sql = "select * from libros"
        var params = []
        db.all(sql, params, (err, rows) => {
            if (err) {
                res.status(400).json({ "error": err.message });
                return;
            }
            setTimeout(() => {
                res.json({
                    "message": "success",
                    "data": rows
                })
            }, 2000);
        });
    });

    app.get("/api/libros/:id", (req, res, next) => {
        var sql = "select * from libros where id = ?"
        var params = [req.params.id]
        db.get(sql, params, (err, row) => {
            if (err) {
                res.status(400).json({ "error": err.message });
                return;
            }
            setTimeout(() => {
                res.json({
                    "message": "success",
                    "data": row
                })
            }, 1000);
        });
    });

    // app.post("/api/libros", middleware.ensureToken, async (req, res, next) => {
    app.post("/api/libros", async (req, res, next) => {

        var errors = []
        if (!req.body.nombre) {
            errors.push("No name specified");
        }
        if (!req.body.tipo) {
            errors.push("No tipe specified");
        }
        if (!req.body.precio) {
            errors.push("No price specified");
        }
        if (errors.length) {
            res.status(400).json({ "error": errors.join(",") });
            return;
        }
        var data = {
            nombre: req.body.nombre,
            tipo: req.body.tipo,
            precio: req.body.precio,
        }
        var sql = 'INSERT INTO libros (nombre, tipo, precio) VALUES (?,?,?)'
        var params = [data.nombre, data.tipo, data.precio]
        db.run(sql, params, function (err, result) {
            if (err) {
                res.status(400).json({ "error": err.message })
                return;
            }
            res.json({
                "message": "success",
                "data": data,
                "id": this.lastID
            })
        });
    })



    // app.put("/api/libros/:id", middleware.ensureToken, async (req, res, next) => {
    app.put("/api/libros/:id", async (req, res, next) => {

        var errors = []
        if (!req.body.nombre) {
            errors.push("No name specified");
        }
        if (!req.body.tipo) {
            errors.push("No tipe specified");
        }
        if (!req.body.precio) {
            errors.push("No price specified");
        }
        if (errors.length) {
            res.status(400).json({ "error": errors.join(",") });
            return;
        }
        var data = {
            nombre: req.body.nombre,
            tipo: req.body.tipo,
            precio: req.body.precio,
        }
        db.run(
            `UPDATE libros set 
               nombre = coalesce(?,nombre), 
               tipo = COALESCE(?,tipo), 
               precio = coalesce(?,precio) 
               WHERE id = ?`, [data.nombre, data.tipo, data.precio, req.params.id],
            (err, result) => {
                if (err) {
                    res.status(400).json({ "error": res.message })
                    return;
                }
                res.json({
                    message: "success",
                    data: data
                })
            });
    })

    // app.delete("/api/libros/:id", middleware.ensureToken, (req, res, next) => {
    app.delete("/api/libros/:id", (req, res, next) => {

        db.run(
            'DELETE FROM libros WHERE id = ?',
            req.params.id,
            function (err, result) {
                if (err) {
                    res.status(400).json({ "error": res.message })
                    return;
                }
                res.json({ "message": "deleted", rows: this.changes })
            });
    })


    // Root path
    app.get("/", (req, res, next) => {
        res.json({ "message": "Ok" })
    });
}


module.exports = librosModule;